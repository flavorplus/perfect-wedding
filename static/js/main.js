/*jshint esversion: 6 */ (function () {
  "use strict";

  // iPad and iPod detection
  var isiPad = function () {
    return navigator.platform.indexOf("iPad") != -1;
  };

  var isiPhone = function () {
    return (
      navigator.platform.indexOf("iPhone") != -1 ||
      navigator.platform.indexOf("iPod") != -1
    );
  };

  var fullHeight = function () {
    if (!isiPad() || !isiPhone()) {
      $(".js-fullheight-home").css(
        "height",
        $(window).height() - $(".fh5co-main-nav").height()
      );
      $(window).resize(function () {
        $(".js-fullheight-home").css(
          "height",
          $(window).height() - $(".fh5co-main-nav").height()
        );
      });
    }
  };

  // Loading page
  var loaderPage = function () {
    $(".fh5co-loader").fadeOut("slow");
    if ($(".save-the-date").length) {
      $(".save-the-date").addClass("popup-save-the-date");
    }
  };

  var fh5coTabs = function () {
    // $('.fh5co-tabs-container').
    $(".fh5co-tabs li a").click(function (event) {
      event.preventDefault();
      var $this = $(this),
        tab = $this.data("tab");
      $(".fh5co-tabs li").removeClass("active");
      $this.closest("li").addClass("active");
      $this
        .closest(".fh5co-tabs-container")
        .find(".fh5co-tab-content")
        .removeClass("active");
      $this
        .closest(".fh5co-tabs-container")
        .find('.fh5co-tab-content[data-tab-content="' + tab + '"]')
        .addClass("active");
    });
  };

  var gridAutoHeight = function () {
    if (!isiPhone() || !isiPad()) {
      $(".fh5co-grid-item").css(
        "height",
        $(".fh5co-2col-inner").outerHeight() / 2
      );
    }
    $(window).resize(function () {
      if (!isiPhone() && !isiPad()) {
        $(".fh5co-grid-item").css(
          "height",
          $(".fh5co-2col-inner").outerHeight() / 2
        );
      }
    });
  };

  var sliderSayings = function () {
    $("#fh5co-sayings .flexslider").flexslider({
      animation: "slide",
      slideshowSpeed: 5000,
      directionNav: false,
      controlNav: true,
      smoothHeight: true,
      reverse: true,
    });
  };

  var offcanvasMenu = function () {
    $("body").prepend('<div id="fh5co-offcanvas" />');
    $("body").prepend(
      '<a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>'
    );

    $(".fh5co-main-nav .fh5co-menu-1 a, .fh5co-main-nav .fh5co-menu-2 a").each(
      function () {
        var $this = $(this);

        $("#fh5co-offcanvas").append($this.clone());
      }
    );
    // $('#fh5co-offcanvas').append
  };

  var mainMenuSticky = function () {
    var sticky = $(".js-sticky");

    sticky.css("height", sticky.height());
    $(window).resize(function () {
      sticky.css("height", sticky.height());
    });

    var $section = $(".fh5co-main-nav");

    $section.waypoint(
      function (direction) {
        if (direction === "down") {
          $section
            .css({
              position: "fixed",
              top: 0,
              width: "100%",
              "z-index": 99999,
            })
            .addClass("fh5co-shadow");
        }
      },
      {
        offset: "0px",
      }
    );

    $(".js-sticky").waypoint(
      function (direction) {
        if (direction === "up") {
          $section.attr("style", "").removeClass("fh5co-shadow");
        }
      },
      {
        offset: function () {
          return -$(this.element).height() + 69;
        },
      }
    );
  };

  // Parallax
  var parallax = function () {
    // $(window).stellar();
    if (!isiPhone() || isiPad()) {
      $(window).stellar({
        horizontalScrolling: false,
      });
    }
  };

  // Burger Menu
  var burgerMenu = function () {
    $("body").on("click", ".js-fh5co-nav-toggle", function (event) {
      var $this = $(this);

      if ($("body").hasClass("offcanvas-visible")) {
        $("body").removeClass("offcanvas-visible fh5co-overflow");
        $this.removeClass("active");
      } else {
        $("body").addClass("offcanvas-visible fh5co-overflow");
        $this.addClass("active");
      }

      event.preventDefault();
    });
  };

  var scrolledWindow = function () {
    $(window).scroll(function () {
      var scrollPos = $(this).scrollTop();

      if ($("body").hasClass("offcanvas-visible")) {
        $("body").removeClass("offcanvas-visible");
        $(".js-fh5co-nav-toggle").removeClass("active");
      }
    });

    $(window).resize(function () {
      if ($("body").hasClass("offcanvas-visible")) {
        $("body").removeClass("offcanvas-visible");
        $(".js-fh5co-nav-toggle").removeClass("active");
      }
    });
  };

  // Click outside of offcanvass
  var mobileMenuOutsideClick = function () {
    $(document).click(function (e) {
      var container = $("#fh5co-offcanvas, .js-fh5co-nav-toggle");
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        if ($("body").hasClass("offcanvas-visible")) {
          $("body").removeClass("offcanvas-visible");
          $(".js-fh5co-nav-toggle").removeClass("active");
        }
      }
    });
  };

  var goToTop = function () {
    $(".js-gotop").on("click", function (event) {
      event.preventDefault();

      $("html, body").animate(
        {
          scrollTop: $("html").offset().top,
        },
        500,
        "easeInOutExpo"
      );

      return false;
    });

    $(window).scroll(function () {
      var $win = $(window);
      if ($win.scrollTop() > 200) {
        $(".js-top").addClass("active");
      } else {
        $(".js-top").removeClass("active");
      }
    });
  };

  // Page Nav
  var clickMenu = function () {
    var topVal = $(window).width() < 769 ? 0 : 58;

    $(window).resize(function () {
      topVal = $(window).width() < 769 ? 0 : 58;
    });
    $(
      '.fh5co-main-nav a:not([class="external"]), #fh5co-offcanvas a:not([class="external"]), a.fh5co-content-nav:not([class="external"]), button.fh5co-content-nav:not([class="external"])'
    ).click(function (event) {
      var section = $(this).data("nav-section");

      if ($('div[data-section="' + section + '"]').length) {
        $("html, body").animate(
          {
            scrollTop:
              $('div[data-section="' + section + '"]').offset().top - topVal,
          },
          500,
          "easeInOutExpo"
        );
      }

      event.preventDefault();

      // return false;
    });
  };

  // Reflect scrolling in navigation
  var navActive = function (section) {
    $(
      ".fh5co-main-nav a[data-nav-section], #fh5co-offcanvas a[data-nav-section]"
    ).removeClass("active");
    $(".fh5co-main-nav, #fh5co-offcanvas")
      .find('a[data-nav-section="' + section + '"]')
      .addClass("active");
  };

  var navigationSection = function () {
    var $section = $("div[data-section]");

    $section.waypoint(
      function (direction) {
        if (direction === "down") {
          navActive($(this.element).data("section"));
        }
      },
      {
        offset: "150px",
      }
    );

    $section.waypoint(
      function (direction) {
        if (direction === "up") {
          navActive($(this.element).data("section"));
        }
      },
      {
        offset: function () {
          return -$(this.element).height() + 155;
        },
      }
    );
  };

  var dynamicForm = function () {
    var oldPersonsCount = 0;
    $("#inputPersons").on("change", function (event) {
      var newPersonsCount = Number($("#inputPersons").val());

      if (oldPersonsCount > newPersonsCount) {
        for (var i = oldPersonsCount; i > newPersonsCount; i--) {
          oldPersonsCount--;
          var reference = ".person" + (i - 1);
          $(reference).hide(400, function () {
            $(this).remove();
          });
        }
      } else if (oldPersonsCount < newPersonsCount) {
        for (var i = oldPersonsCount; i < newPersonsCount; i++) {
          var template = $(".template")
            .clone()
            .removeClass("template")
            .addClass("jsControl")
            .addClass("person" + i);

          var personID = i + 1;
          var field;
          field = template.find("#inputNameTemplate");
          field.prop("id", "namePerson" + personID);
          field.prop("name", "namePerson" + personID);
          field.prop("required", true);
          field.attr("placeholder", field.attr("placeholder") + " " + personID);

          field = template.find("#inputBirthdayTemplate");
          field.prop("id", "birthdayPerson" + personID);
          field.prop("name", "birthdayPerson" + personID);
          field.attr("placeholder", field.attr("placeholder") + " " + personID);

          field = template.find("#inputDietTemplate");
          field.prop("id", "dietPerson" + personID);
          field.prop("name", "dietPerson" + personID);
          field
            .find("#default")
            .text(field.find("#default").text() + " " + personID);

          oldPersonsCount++;
          $(".jsControl").last().after(template);
          $(".jsControl").show(400);
        }
      }

      datepicker();
    });
  };

  const scriptURL =
    "https://script.google.com/macros/s/AKfycbzjwZZt7dS0xOln9E6cGFo7Glx2udVAlXJDMKUTUYjT95c1UHmLt1Bkh2VGr7vMVoq4/exec";
  const form = document.forms["contact-form"];
  const loading = document.querySelector(".js-loading");
  const successMessage = document.querySelector(".js-success-message");
  const errorMessage = document.querySelector(".js-error-message");

  form.addEventListener("submit", (e) => {
    e.preventDefault();
    showLoadingIndicator();
    fetch(scriptURL, {
      method: "POST",
      body: new FormData(form),
    })
      .then((response) => showSuccessMessage(response))
      .catch((error) => showErrorMessage(error));
  });

  function showLoadingIndicator() {
    form.classList.add("hidden");
    loading.classList.remove("hidden");
  }

  function showSuccessMessage(response) {
    console.log("Success!", response.json());
    setTimeout(() => {
      successMessage.classList.remove("hidden");
      loading.classList.add("hidden");
    }, 500);
  }

  function showErrorMessage(error) {
    console.error("Error!", error.message);
    setTimeout(() => {
      errorMessage.classList.remove("hidden");
      loading.classList.add("hidden");
    }, 500);
  }

  function datepicker() {
    $("input.date").datepicker({
      format: "dd-mm-yyyy",
      startView: 2,
      autoclose: true,
      orientation: "bottom auto",
    });
  }

  // Document on load.
  $(function () {
    fullHeight();
    loaderPage();
    fh5coTabs();
    gridAutoHeight();

    // sliderMain();
    // sliderSayings();
    offcanvasMenu();
    mainMenuSticky();
    parallax();
    burgerMenu();
    scrolledWindow();
    mobileMenuOutsideClick();
    clickMenu();
    navigationSection();
    goToTop();
    dynamicForm();
    datepicker();
  });
})();
